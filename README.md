# Async TCP File Server

This is an asynchronous version of [TCP file server](https://gitlab.com/fyne2/tcp-file-server) demonstrating event based programming model. This async server utilizes POSIX asynchronous I/O interface `aio` to handle multiple client requests concurrently without blocking and is notified of I/O completion using the delivery of signals.

![async requests](async.png)

## Features

- Socket Creation and Configuration: The server creates a `socket()` to listen for incoming connections and configures it for non-blocking operation using `ioctl()`.
- Connection Handling: It accepts incoming connections from clients using `connect()`
- Manages multiple connections concurrently using the `select()` system call.
- File Transfer: Handles I/O operations asynchronously using AIO functions (`aio_read()` and `aio_write()`) and aiocb structure. 
- Signal Handling: Registers a signal handler `aioSigHandler` to handle asynchronous I/O completion signals.


## Usage
- Compile code with `make` command

- Execute script to start file server and simulate client requests
`./run.sh`
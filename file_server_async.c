#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <aio.h>
#include <signal.h>


#define FALSE           0
#define BUFFER_SIZE     1024
#define TRUE            1
#define SERVER_PORT     8888
#define IO_SIGNAL SIGUSR1  /* Signal used to notify completion */

struct ioRequest {
   int            req_num;
   ssize_t        bytes_read;
   off_t          file_size;
   int            client_sockfd;
   int            close_conn;
   int            read_status;
   int            write_status;
   int            read_complete;
   int            write_complete;
   int            operation;     /* 0: read, 1: write */
   struct aiocb   *read_aiocbp;
   struct aiocb   *write_aiocbp;
};

static volatile sig_atomic_t aio_completion_flag = 0;

// Set completion flag on delivery of signal
void aioSigHandler(int sig, siginfo_t *si, void *ucontext)
{
   if (si->si_code == SI_ASYNCIO) {
      printf("I/O completion signalled\n");
      aio_completion_flag = 1;
   }
}

// Clean-up aio_completion
void handle_aio_completion(struct ioRequest *io_req)
{
   int rc;
   rc = close(io_req->read_aiocbp->aio_fildes);
   if (rc < 0)
   {
      fprintf(stderr, "failed to close aio_fildes: %d\n", io_req->read_aiocbp->aio_fildes);
   }
   io_req->read_aiocbp->aio_nbytes = 0;
   io_req->read_aiocbp->aio_offset = 0;
   io_req->read_aiocbp->aio_sigevent.sigev_notify = SIGEV_NONE;
   io_req->read_aiocbp->aio_sigevent.sigev_signo = 0;
   io_req->read_aiocbp->aio_sigevent.sigev_value.sival_ptr = NULL;
   io_req->read_aiocbp->aio_buf = NULL;
   io_req->write_aiocbp->aio_buf = NULL;
   io_req->close_conn = TRUE;
   io_req->read_status = 0;
}

/* TODO: Clean up resources */


int main (int argc, char *argv[])
{
   int    i, len, rc, on = 1;
   int    listen_sd, max_sd, new_sd;
   int    maxnum_reqs=30, open_reqs=0;
   int    desc_ready, end_server = FALSE;
   int    close_conn;
   char   buffer[BUFFER_SIZE] = { 0 };
   struct sockaddr_in addr;
   struct timeval      timeout;
   fd_set              master_set, working_set;
   struct sigaction sa;
   sigset_t sig_mask;

   struct ioRequest *ioList = calloc(maxnum_reqs, sizeof(*ioList));
   if (ioList == NULL)
   {
      perror("calloc");
      exit(-1);
   }

   struct aiocb *read_aiocbList = calloc(maxnum_reqs, sizeof(*read_aiocbList));
   if (read_aiocbList == NULL)
   {
      perror("calloc");
      exit(-1);
   }

   struct aiocb *write_aiocbList = calloc(maxnum_reqs, sizeof(*write_aiocbList));
   if (write_aiocbList == NULL)
   {
      perror("calloc");
      exit(-1);
   }

   /* Handlers for I/O completion signal */
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = SA_RESTART | SA_SIGINFO;
   sa.sa_sigaction = aioSigHandler;
   if (sigaction(IO_SIGNAL, &sa, NULL) == -1)
   {
      perror("sigaction");
      exit(-1);
   }

   /*************************************************************/
   /* Create an AF_INET stream socket to receive incoming      */
   /* connections on                                            */
   /*************************************************************/
   listen_sd = socket(AF_INET, SOCK_STREAM, 0);
   if (listen_sd < 0)
   {
      perror("socket() failed");
      exit(-1);
   }

   /*************************************************************/
   /* Allow socket descriptor to be reuseable                   */
   /*************************************************************/
   rc = setsockopt(listen_sd, SOL_SOCKET,  SO_REUSEADDR,
                   (char *)&on, sizeof(on));
   if (rc < 0)
   {
      perror("setsockopt() failed");
      close(listen_sd);
      exit(-1);
   }

   /*************************************************************/
   /* Set socket to be nonblocking. All of the sockets for      */
   /* the incoming connections will also be nonblocking since   */
   /* they will inherit that state from the listening socket.   */
   /*************************************************************/
   rc = ioctl(listen_sd, FIONBIO, (char *)&on);
   if (rc < 0)
   {
      perror("ioctl() failed");
      close(listen_sd);
      exit(-1);
   }

   /*************************************************************/
   /* Bind the socket                                           */
   /*************************************************************/
   memset(&addr, 0, sizeof(addr));
   addr.sin_family      = AF_INET;
   addr.sin_addr.s_addr = INADDR_ANY;
   //memcpy(&addr.sin_addr.s_addr, &i, sizeof(in6addr_any));
   addr.sin_port        = htons(SERVER_PORT);
   rc = bind(listen_sd,
             (struct sockaddr *)&addr, sizeof(addr));
   if (rc < 0)
   {
      perror("bind() failed");
      close(listen_sd);
      exit(-1);
   }

   /*************************************************************/
   /* Set the listen back log                                   */
   /*************************************************************/
   rc = listen(listen_sd, 32);
   if (rc < 0)
   {
      perror("listen() failed");
      close(listen_sd);
      exit(-1);
   }

   /*************************************************************/
   /* Initialize the master fd_set                              */
   /*************************************************************/
   FD_ZERO(&master_set);
   max_sd = listen_sd;
   FD_SET(listen_sd, &master_set);

   /*************************************************************/
   /* Initialize the timeval struct to 3 minutes.  If no        */
   /* activity after 3 minutes this program will end.           */
   /*************************************************************/
   timeout.tv_sec  = 3 * 60;
   timeout.tv_usec = 0;

   /*************************************************************/
   /* Loop waiting for incoming connects or for incoming data   */
   /* on any of the connected sockets.                          */
   /*************************************************************/
   do
   {
      /**********************************************************/
      /* Copy the master fd_set over to the working fd_set.     */
      /**********************************************************/
      memcpy(&working_set, &master_set, sizeof(master_set));


      if (aio_completion_flag)
      {
         for (i=0; i < open_reqs; i++)
         {
            ssize_t n_read;
            if (ioList[i].read_status == EINPROGRESS)
            {
               ioList[i].read_status = aio_error(ioList[i].read_aiocbp);
            }

            switch (ioList[i].read_status)
            {
               case 0:
                  printf("Client %d read completed\n", ioList[i].client_sockfd);
                  n_read = aio_return(ioList[i].read_aiocbp);
                  ioList[i].bytes_read += n_read;
                  if (n_read == 0 || ioList[i].bytes_read >= ioList[i].file_size)
                  {
                     printf("File read on client %d completed\n", ioList[i].client_sockfd);
                     ioList[i].read_complete = TRUE;
                  }
                  ioList[i].operation = 1;
                  break;
               
               case EINPROGRESS:
                  printf("Client %d in progress\n", ioList[i].client_sockfd);
                  break;
               case ECANCELED:
                  printf("Client %d I/O request cancelled\n", ioList[i].client_sockfd);
                  handle_aio_completion(&ioList[i]);
                  break;
            }

            if (ioList[i].write_status == EINPROGRESS)
            {
               ioList[i].write_status = aio_error(ioList[i].write_aiocbp);
            }

            switch (ioList[i].write_status)
            {
               case 0:
                  printf("Server ready to write to client %d\n", ioList[i].client_sockfd);
                  if (ioList[i].read_complete && ioList[i].operation == 0) {
                     printf("File write on client %d completed\n", ioList[i].client_sockfd);
                     ioList[i].write_complete = TRUE;
                  }
                  
                  if (ioList[i].operation == 1)
                  {
                     memcpy(&ioList[i].write_aiocbp->aio_buf, &ioList[i].read_aiocbp->aio_buf, n_read);
                     // Set the number of bytes to be written
                     ioList[i].write_aiocbp->aio_nbytes = n_read;
                     rc = aio_write(ioList[i].write_aiocbp);
                     printf("sent write successfully\n");
                     if (rc < 0)
                     {
                        perror("aio_write()");
                        break;
                     }
                     ioList[i].operation = 0;
                  }

               case EINPROGRESS:
                  printf("Write still in-progress...\n");
                  break;
               
               case ECANCELED:
                  printf("Write was cancelled\n");
                  break;
            }
            if (ioList[i].read_complete && ioList[i].write_complete)
            {
               printf("Sent client %d for cleanup\n",ioList[i].client_sockfd);
               handle_aio_completion(&ioList[i]);
            }
         }
         aio_completion_flag = 0;
      }


      /**********************************************************/
      /* Call select() and wait 3 minutes for it to complete.   */
      /**********************************************************/
      
      sigemptyset(&sig_mask);
      sigaddset(&sig_mask, IO_SIGNAL);

      sigprocmask(SIG_BLOCK, &sig_mask, NULL);
      

      printf("Waiting on select()...\n");
      rc = select(max_sd + 1, &working_set, NULL, NULL, &timeout);
      sigprocmask(SIG_UNBLOCK, &sig_mask, NULL);

      /**********************************************************/
      /* Check to see if the select call failed.                */
      /**********************************************************/
      if (rc < 0)
      {
         perror("  select() failed");
         break;
      }

      /**********************************************************/
      /* Check to see if the 3 minute time out expired.         */
      /**********************************************************/
      if (rc == 0)
      {
         printf("  select() timed out.  End program.\n");
         break;
      }

      /**********************************************************/
      /* One or more descriptors are readable.  Need to         */
      /* determine which ones they are.                         */
      /**********************************************************/
      desc_ready = rc;
      for (i=0; i <= max_sd  &&  desc_ready > 0; ++i)
      {
         /*******************************************************/
         /* Check to see if this descriptor is ready            */
         /*******************************************************/
         if (FD_ISSET(i, &working_set))
         {
            /****************************************************/
            /* A descriptor was found that was readable - one   */
            /* less has to be looked for.  This is being done   */
            /* so that we can stop looking at the working set   */
            /* once we have found all of the descriptors that   */
            /* were ready.                                      */
            /****************************************************/
            desc_ready -= 1;

            /****************************************************/
            /* Check to see if this is the listening socket     */
            /****************************************************/
            if (i == listen_sd)
            {
               printf("  Listening socket is readable\n");
               /*************************************************/
               /* Accept all incoming connections that are      */
               /* queued up on the listening socket before we   */
               /* loop back and call select again.              */
               /*************************************************/
               do
               {
                  // Check if to add new_server...
                  if (open_reqs == maxnum_reqs) {
                     fprintf(stderr, "Server capacity full cannot accept anymore requests\n");
                     sleep(3);
                     break;
                  }

                  /**********************************************/
                  /* Accept each incoming connection.  If       */
                  /* accept fails with EWOULDBLOCK, then we     */
                  /* have accepted all of them.  Any other      */
                  /* failure on accept will cause us to end the */
                  /* server.                                    */
                  /**********************************************/
                  new_sd = accept(listen_sd, NULL, NULL);
                  if (new_sd < 0)
                  {
                     if (errno != EWOULDBLOCK)
                     {
                        perror("  accept() failed");
                        end_server = TRUE;
                     }
                     break;
                  }


                  /**********************************************/
                  /* Add the new incoming connection to the     */
                  /* master read set                            */
                  /**********************************************/
                  printf("  New incoming connection - %d\n", new_sd);
                  FD_SET(new_sd, &master_set);
                  if (new_sd > max_sd)
                     max_sd = new_sd;
                  
                  ioList[open_reqs].client_sockfd = new_sd;
                  ioList[open_reqs].req_num = open_reqs + 1;
                  ioList[open_reqs].close_conn = FALSE;
                  ioList[open_reqs].bytes_read = 0;
                  ioList[open_reqs].read_complete = FALSE;
                  ioList[open_reqs].write_complete = FALSE;
                  ioList[open_reqs].operation = 0;
                  
                  /**********************************************/
                  /* Receive data on this connection until the  */
                  /* recv fails with EWOULDBLOCK.  If any other */
                  /* failure occurs, we will close the          */
                  /* connection.                                */
                  /**********************************************/
                  rc = recv(new_sd, buffer, sizeof(buffer) - 1, 0);
                  if (rc < 0)
                  {
                    if (errno != EWOULDBLOCK)
                    {
                        perror("   recv() failed");
                        close_conn = TRUE;
                    }
                    break;
                  }

                  /**********************************************/
                  /* Check to see if the connection has been    */
                  /* closed by the client                       */
                  /**********************************************/
                  if (rc == 0)
                  {
                    printf("  Connection closed\n");
                    close_conn = TRUE;
                    break;
                  } else {
                    buffer[rc] = '\0';
                  }

                  /*************************************************/
                  /* Data was received                              */
                  /*************************************************/
                  len = rc;
                  printf("  %d bytes received\n", len);

                  /**********************************************/
                  /* Open file for reading asynchronously       */
                  /**********************************************/
                  int fd = open(buffer, O_RDONLY);
                  if (fd == -1)
                  {
                     perror("open");
                     exit(-1);
                  }
                  printf("Opened file %s on fd %d\n", buffer, fd);
                  struct stat file_stat;
                  if (fstat(fd, &file_stat) < 0)
                  {
                     perror("fstat");
                     exit(-1);
                  }
                  ioList[open_reqs].file_size = file_stat.st_size;

                  ioList[open_reqs].read_aiocbp = &read_aiocbList[open_reqs];
                  ioList[open_reqs].write_aiocbp = &write_aiocbList[open_reqs];
                  ioList[open_reqs].read_aiocbp->aio_fildes = fd;
                  ioList[open_reqs].write_aiocbp->aio_fildes = new_sd;
                  ioList[open_reqs].read_status = EINPROGRESS;
                  ioList[open_reqs].write_status = EINPROGRESS;
                  ioList[open_reqs].read_aiocbp->aio_buf = malloc(BUFFER_SIZE);
                  ioList[open_reqs].write_aiocbp->aio_buf = malloc(BUFFER_SIZE);
                  ioList[open_reqs].read_aiocbp->aio_nbytes = BUFFER_SIZE;
                  ioList[open_reqs].write_aiocbp->aio_nbytes = BUFFER_SIZE;
                  ioList[open_reqs].read_aiocbp->aio_offset = 0;
                  ioList[open_reqs].write_aiocbp->aio_offset = 0;
                  ioList[open_reqs].read_aiocbp->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
                  ioList[open_reqs].write_aiocbp->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
                  ioList[open_reqs].read_aiocbp->aio_sigevent.sigev_signo = IO_SIGNAL;
                  ioList[open_reqs].write_aiocbp->aio_sigevent.sigev_signo = IO_SIGNAL;
                  ioList[open_reqs].read_aiocbp->aio_sigevent.sigev_value.sival_ptr = &ioList[open_reqs];
                  ioList[open_reqs].write_aiocbp->aio_sigevent.sigev_value.sival_ptr = &ioList[open_reqs];

                  ioList[open_reqs].read_status = aio_read(ioList[open_reqs].read_aiocbp);
                  printf("AIO read status %d\n", ioList[open_reqs].read_status);
                  if (ioList[open_reqs].read_status != 0)
                  {
                     perror("aio_read");
                     close(fd);
                     exit(-1);
                  }



                  /**********************************************/
                  /* Loop back up and accept another incoming   */
                  /* connection                                 */
                  /**********************************************/
                  open_reqs += 1;
               } while (new_sd != -1);
            }

            /****************************************************/
            /* This is not the listening socket, therefore an   */
            /* existing connection must be readable             */
            /****************************************************/
            else
            {
               if (ioList[i].close_conn) 
               {
                  rc = close(ioList[i].client_sockfd);
                  if (rc < 0)
                  {
                    perror("close");
                  }
                  printf("Client %d connection closed\n", ioList[i].client_sockfd);
                  open_reqs--;
                  FD_CLR(ioList[i].client_sockfd, &master_set);
                  if (ioList[i].client_sockfd == max_sd)
                  {
                     while (FD_ISSET(max_sd, &master_set) == FALSE)
                        max_sd -= 1;
                  }
                  /* Reset ioList for re-use*/
                  ioList[i].close_conn = FALSE;
               }
               else if (aio_error(ioList[i].read_aiocbp) == 0)
               {
                 ioList[i].read_status = aio_read(ioList[i].read_aiocbp);
                 printf("AIO read status %d\n", ioList[open_reqs].read_status);
                  if (ioList[open_reqs].read_status != 0)
                  {
                     perror("aio_read");
                     close(ioList[i].read_aiocbp->aio_fildes);
                     exit(-1);
                  }
               }
            }   /* End of existing connection is readable */
         }  /* End of if (FD_ISSET(i, &working_set)) */
      } /* End of loop through selectable descriptors */
   } while (end_server == FALSE);

   /*************************************************************/
   /* Clean up all of the sockets that are open                 */
   /*************************************************************/
   for (i=0; i <= max_sd; ++i)
   {
      if (FD_ISSET(i, &master_set))
         close(i);
   }

   exit(EXIT_SUCCESS);
}
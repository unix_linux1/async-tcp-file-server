all: file_server_async client_server

clean:
	rm -f file_server_async client_server

file_server_async: file_server_async.c
	gcc -o file_server_async file_server_async.c -Wall

client_server: client_server.c
	gcc -o client_server client_server.c -Wall